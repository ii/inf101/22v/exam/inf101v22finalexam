# 1-4

Oppgavetekst: [bokmål](./inf101v22-finalexam-bokmal.pdf)

# 5 Card

Oppgavetekst: [bokmål](./cards-bokmal.md), [nynorsk](./cards-nynorsk.md), [english](./cards-english.md).

# 6 Whac-a-mole

Guide: [bokmål](./whacamole-bokmal.md), [nynorsk](./whacamole-nynorsk.md), [english](./whacamole-english.md).


# Sensorveiledning

Ligger i [sensorveiledning.pdf](./sensorveiledning.pdf).